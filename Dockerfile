FROM ubuntu:17.04

MAINTAINER benjamin.dittwald@fokus.fraunhofer.de

RUN apt-get update && apt-get -y install openjdk-8-jre openjdk-8-jdk openjfx gradle maven git